# Compton Configuration

Compton configuration (fizzy-compliant)

## Documentation

See [wiki](https://gitlab.com/fizzycfg/official-cfg/configs-compton/wikis/home)

## Contributing

See [CONTRIBUTING](./CONTRIBUTING.md)

## License

See [LICENSE](./LICENSE)
